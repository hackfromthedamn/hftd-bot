#!/usr/bin/env python

from hftd.DefaultImports import *

import dotenv
import hftd

logging.basicConfig(level = logging.INFO)
logging.captureWarnings(True)

dotenv.load_dotenv()


class EnvironmentVariableNotSetException(Exception):
    pass


def configure(binder: injector.Binder):
    binder.install(hftd.Module())
    discord_token = os.getenv("DISCORD_TOKEN")
    if discord_token is None:
        raise EnvironmentVariableNotSetException("The DISCORD_TOKEN environment variable must be set")
    binder.bind(hftd.Token, to = discord_token)

    bot_data_path = os.getenv("BOT_DATA_PATH")
    if bot_data_path is None:
        bot_data_path = Path(__file__).parent.absolute().joinpath("data")

    binder.bind(hftd.DataPath, to = Path(bot_data_path))

injector = injector.Injector(configure)
bot = injector.get(hftd.Bot)
bot.add_cogs(
    injector.get(hftd.cogs.Bot),
    injector.get(hftd.cogs.Money),
    injector.get(hftd.cogs.Fame),
    injector.get(hftd.meetingrooms.MeetingroomCog),
    injector.get(hftd.cogs.Search)
)

servers_cog = hftd.servers.ServersCog(bot, injector.get(hftd.GeneralStorage))
bot.add_cogs(servers_cog)


bot.run()

if bot.restarting:
    exit(234)

