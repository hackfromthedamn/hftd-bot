from .Meetingroom import Meetingroom
from ..DefaultImports import *
from .. import Checks
from ..Bot import Bot
import hftd


class MeetingroomCog(commands.Cog, name = "Meetingroom"):
    @injector.inject
    def __init__(self, bot: Bot, storage: hftd.GeneralStorage) -> None:
        self.bot = bot
        self._general_storage = storage
        self.__active_rooms = {}

    @commands.group()
    @Checks.check_admin()
    async def meetingroom(self, context):
        """
        Commands that deal with meeting rooms.
        """
        pass

    @meetingroom.command()
    async def start(self, context, meetingroom_name: str, members: commands.Greedy[discord.Member]) -> None:
        """
        Start a new meeting room.

        This will start a new meetingroom.

        :param meetingroom_name: name of the meetingroom.
        :param members: The members that should be added to the meeting room.
        """
        await context.send(f"Started meeting room {meetingroom_name}")

        meetingroom = Meetingroom(meetingroom_name, members, self._general_storage.data["meeting_room_category"])
        await meetingroom.start(context)
        self.__active_rooms[meetingroom_name] = meetingroom

    @meetingroom.command()
    async def stop(self, context, meetingroom_name: str) -> None:
        """
        Close down a meeting room.
        :param meetingroom_name:
        """
        await self.__active_rooms[meetingroom_name].stop(context)

        await context.send(f"Closed the meeting room {meetingroom_name}")

    @meetingroom.command(name="add-members", aliases=["add_members"])
    async def add_members(self, context, meetingroom_name: str, members: commands.Greedy[discord.Member]) -> None:
        """
        Add more members to a meetingroom

        :param meetingroom_name: The room to add members to.
        :param members: The extra members to add.
        """
        if meetingroom_name not in self.__active_rooms:
            await context.send("No such meetingroom.")
            return

        await self.__active_rooms[meetingroom_name].add_members(context, members)

    @meetingroom.command(name="remove-members", aliases=["remove_members"])
    async def remove_members(self, context, meetingroom_name: str, members: commands.Greedy[discord.Member]) -> None:
        """
        Remove members from a meetingroom.

        :param meetingroom_name: The room to remove members from.
        :param members: The members to remove.
        """
        if meetingroom_name not in self.__active_rooms:
            await context.send("No such meetingroom.")
            return

        await self.__active_rooms[meetingroom_name].remove_members(context, members)
