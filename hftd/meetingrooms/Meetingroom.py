import logging
import discord
from discord.ext import commands
from typing import List

from hftd.ChannelWrapper import ChannelWrapper
from hftd.Room import Room

log = logging.getLogger("Meetingroom")


class Meetingroom(Room):
    def __init__(self, name: str, members: List[discord.Member], category: str) -> None:
        super().__init__(name, members, category)

    async def start(self, context: commands.Context) -> None:
        await super().start(context)

        overwrites = {
            context.guild.default_role: discord.PermissionOverwrite(
                read_messages = False, send_messages = False,
                connect = False, speak = False, view_channel = False
            )
        }

        text_channel = await context.guild.create_text_channel(self._name, category = self._room_category, overwrites = overwrites)
        self._channel_wrappers.append(ChannelWrapper(text_channel))

        await self._add_initial_members(context)

        log.info(f"Started meeting room {self._name}")
        log.info(f"  Members: {','.join(map(lambda m: m.display_name, self._members))}")