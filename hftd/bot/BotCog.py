from ..DefaultImports import *

from .. import Checks

class BotCog(commands.Cog, name = "Bot"):
    def __init__(self, bot: commands.Bot) -> None:
        self.bot = bot

    @commands.group()
    @Checks.check_admin()
    async def bot(self, context: commands.Context) -> None:
        pass

    @bot.command()
    async def status(self, context: commands.Context) -> None:
        await context.send("Bot running")
        if "Servers" in self.bot.cogs:
            active = self.bot.get_cog("Servers").active_server_count
            await context.send(f"Servers active: {active}")

    @bot.command()
    async def say(self, context, channel: discord.TextChannel, *, message: str) -> None:
        await channel.send(message)
