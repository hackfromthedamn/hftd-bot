from typing import Optional
import pathlib

from .. import Storage

class Character:
    def __init__(self, path: Optional[pathlib.Path] = None) -> None:
        self.member = ""
        self.name = ""
        self.collective = ""
        self.description = ""
        self.is_creating = False
        self.path = path
        self.dirty = False

        self.__storage = None
        if path:
            self.__storage = Storage.Storage(path)

    def is_complete(self):
        return self.member and self.name and self.collective and self.description

    async def load(self):
        if not self.__storage:
            return

        await self.__storage.load()

        self.member = self.__storage.data["member"]
        self.name = self.__storage.data["name"]
        self.collective = self.__storage.data["collective"]
        self.description = self.__storage.data["description"]

    async def save(self):
        if not self.dirty:
            return

        if not self.__storage:
            if not self.path:
                return

            self.__storage = Storage.Storage(self.path)

        data = {
            "member": str(self.member),
            "name": self.name,
            "collective": self.collective,
            "description": self.description
        }
        self.__storage.data = data
        await self.__storage.save()
        self.dirty = False
