from typing import List

import discord


class ChannelWrapper:
    def __init__(self, channel):
        """
        A channel wrapper is a small management wrapper for a channel in discord.
        It allows for permissions to be set & removed.
        :param channel:
        """
        self.__channel = channel

    async def stop(self):
        try:
            await self.__channel.delete()
        except discord.NotFound:
            pass

    async def add_permissions(self, members: List[discord.Member]) -> None:
        overwrites = self.__channel.overwrites
        for member in members:
            overwrites[member] = discord.PermissionOverwrite(read_messages=True, send_messages=True)
        await self.__channel.edit(overwrites=overwrites)

    async def remove_permissions(self, members: List[discord.Member]) -> None:
        overwrites = self.__channel.overwrites
        for member in members:
            if member in overwrites:
                del overwrites[member]
        await self.__channel.edit(overwrites = overwrites)