from ..DefaultImports import *

import subprocess
import aiohttp

from ..Bot import Bot
from ..Storage import DataPath
from .. import Checks

@injector.singleton
class BotCog(commands.Cog, name = "Bot"):
    @injector.inject
    def __init__(self, bot: Bot, data_path: DataPath) -> None:
        self.bot = bot
        self.__data_path = data_path
        self.__token = os.getenv("GITLAB_TOKEN")

    @commands.group(case_insensitive = True)
    @Checks.check_admin()
    async def bot(self, context: commands.Context) -> None:
        """
        Generic bot helper commands.
        """
        pass

    @bot.command()
    async def status(self, context: commands.Context) -> None:
        """
        Display the bot's status.
        """
        await context.send("Bot running")
        #if "Servers" in self.bot.cogs:
            #active = self.bot.get_cog("Servers").active_server_count
            #await context.send(f"Servers active: {active}")

    @bot.command()
    async def say(self, context, channel: discord.TextChannel, *, message: str) -> None:
        """
        Say something in a certain channel.

        This will display the message in the specified channel, using the Bot's account.

        :param channel: The channel to display the message in.
        :param message: The message to display. Note that the maximum character limit for messages is 2000.
        """
        await channel.send(message)

    @bot.command()
    async def restart(self, context) -> None:
        """
        Restart the bot.

        This will shut down and restart the bot.
        """

        # Stop and close the event loop, relying on systemd to restart the service.
        await self.bot.close()

        self.bot.restarting = True

    @bot.command(name = "pull-data", aliases = ["pull_data"])
    async def pull_data(self, context) -> None:
        """
        Perform "git pull" on the data directory.

        This is just because I'm lazy and don't want to set up the deployment pipeline
        on Gitlab.
        """
        if self.__token is not None:
            result = await self._check_gitlab_status(context)
            if not result:
                return

        result = subprocess.run(["git", "pull"], capture_output = True, cwd = self.__data_path)
        message = f"```\n{result.stdout.decode()}```"
        await context.send(message)
        message = f"```\n{result.stderr.decode()}```"

    async def _check_gitlab_status(self, context) -> bool:
        headers = {"Private-Token": self.__token}
        async with aiohttp.ClientSession(headers = headers) as session:
            pipeline = None
            async with session.get("https://gitlab.com/api/v4/projects/17657519/pipelines") as response:
                pipeline = (await response.json())[0]

            if pipeline["status"] == "success":
                await context.send("Pipeline status is green, continuing")
                return True
            elif pipeline["status"] == "pending":
                await context.send("Pipeline is running, please wait for it to finish.")
                return False
            elif pipeline["status"] == "failed":
                jobs = []
                async with session.get(f"https://gitlab.com/api/v4/projects/17657519/pipelines/{pipeline['id']}/jobs") as response:
                    jobs = await response.json()

                await context.send("Can not update data, last pipeline contains errors:")
                for job in jobs:
                    if job["status"] != "success":
                        await context.send(f"See {job['web_url']} for details")

                return False
