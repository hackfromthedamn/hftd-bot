
from .Bot import BotCog as Bot
from .Search import Search
from .Money import Money, MoneyModule
from .Fame import Fame, FameModule

import injector

class Module(injector.Module):
    def configure(self, binder: injector.Binder) -> None:
        binder.install(MoneyModule)
        binder.install(FameModule)

