from ..DefaultImports import *

from decimal import Decimal

import aiofiles

from ..Bot import Bot
from ..Serializable import Serializable
from ..Storage import Storage, ValueStorage, DataPath
from .. import Checks

log = logging.getLogger(__name__)

FameConfiguration = NewType("FameConfiguration", Storage)
FameData = NewType("FameData", ValueStorage)

Target = Union[discord.Member, str]

class FameModule(injector.Module):
    @injector.singleton
    @injector.provider
    def configuration(self, data_path: DataPath) -> FameConfiguration:
        return Storage(data_path.joinpath("fame.yml"))

    @injector.singleton
    @injector.provider
    def wallets(self, data_path: DataPath) -> FameData:
        return ValueStorage(data_path.joinpath("fame_data.yml"), data_path.joinpath("fame.log"))

@injector.singleton
class Fame(Serializable, commands.Cog):
    @injector.inject
    def __init__(self, bot: Bot, configuration: FameConfiguration, data: FameData) -> None:
        self.bot = bot

        self.__data = {}
        self.__config = configuration
        self.__data = data
        self.__data.modified.connect(self.target_modified)
        self.__known_targets = []

    @commands.group(case_insensitive = True)
    async def fame(self, context: commands.Context) -> None:
        """
        Commands related to fame.
        """
        pass

    @fame.command()
    @Checks.check_admin()
    async def add(self, context: commands.Context, target: Target, amount: Decimal, *, message: str = "") -> None:
        """
        Add fame to a target.

        This command adds fame to a target. A target can either be a member, a
        troupe or a collective.

        :param target: The target to add fame to.
        :param amount: The amount of fame to add.
        :param message: An optional message that is displayed to the target.
        """
        if isinstance(target, discord.Member):
            target = target.display_name
        elif target not in self.__known_targets:
            await context.send("No such target.")
            return

        log_message = f"{target}'s fame was increased by {amount}."
        await self.__data.modify(target, amount, log_message = log_message, transaction_message = message)
        await context.send(log_message)

    @fame.command()
    @Checks.check_admin()
    async def remove(self, context: commands.Context, target: Target, amount: Decimal, *, message: str = "") -> None:
        """
        Remove fame from a target.

        This command removes fame from a target. A target can either be a member, a
        troupe or a collective.

        :param target: The target to remove fame from.
        :param amount: The amount of fame to remove.
        :param message: An optional message that is displayed to the target.
        """
        if isinstance(target, discord.Member):
            target = target.display_name
        elif target not in self.__known_targets:
            await context.send("No such target.")
            return

        log_message = f"{target}'s fame was decreased by {amount}."
        await self.__data.modify(target, -amount, log_message = log_message, transaction_message = message)
        await context.send(log_message)

    @fame.command()
    async def show(self, context: commands.Context, targets: commands.Greedy[Target]) -> None:
        """
        Show the amount of fame of targets.

        This shows the amount of fame of all the specified targets. If no targets
        are provided, all known fame is displayed.

        :param targets: A list of targets to display fame for.
        """
        if not targets:
            targets = self.__data.entities()

        message = "```\n"
        for t in targets:
            target = t.display_name if isinstance(t, discord.Member) else t
            if self.__data.has_value(target):
                message += f"{target}: {self.__data.value(target)}\n"
            else:
                message += f"{target} has no fame.\n"
        message += "```\n"

        await context.send(message)

    async def load(self) -> None:
        await self.__config.load()
        await self.__data.load()

        self.__known_targets = self.__config.data.get("collectives", []) + self.__config.data.get("troupes", [])

        for target in self.__known_targets:
            if not self.__data.has_value(target):
                await self.__data.set(target, Decimal("0.00"))

    async def save(self) -> None:
        await self.__data.save()

    async def target_modified(self, entity: str, amount: Decimal, message: str) -> None:
        member = self.bot.find_member(entity)
        if not member:
            return

        dm = ""
        if amount > 0:
            dm = f"You received {amount} fame.\n"
        else:
            dm = f"You lost {amount} fame.\n"

        if message:
            dm += message + "\n"

        dm += f"You now have {self.__data.value(entity)} fame."

        await member.send(dm)
