from .. import Storage
from ..DefaultImports import *
from ..Bot import Bot
from ..Serializable import Serializable

import random

log = logging.getLogger(__name__)

Target = str


@injector.singleton
class Search(Serializable, commands.Cog, name = "Search"):
    @injector.inject
    def __init__(self, bot: Bot) -> None:
        self.bot = bot

        self.__search_results = []

        path = Storage.data_path.joinpath("search")
        for file_path in path.iterdir():
            if not file_path.match("*.yml"):
                continue
            self.__search_results.append(SearchResult(file_path))

    async def load(self) -> None:
        for result in self.__search_results:
            await result.load()

    @commands.group(case_insensitive = True)
    async def search(self, context: commands.Context) -> None:
        """
        Commands related to searching the web for information.
        """
        pass

    @search.command()
    async def list(self, context) -> None:
        """
        List all publicly available (pre-indexed) search results. There might be search results that are *not*
        listed here!
        """
        message = "```\n"

        filtered_results = [search_result.result_id for search_result in self.__search_results if search_result.is_public]
        message += "\n".join(filtered_results)

        message += "```\n"

        await context.send(message)

    @search.command()
    async def show(self, context: commands.Context, target: Target) -> None:
        """
        Show a specific search result

        :param target: What do you want to search for?
        """
        result = next((result.formatted_text for result in self.__search_results if result.result_id == target.lower()), "")

        if result:
            message = result
        else:
            message = "```\n Your search did not return any results ```\n"

        await context.send(message)

    @search.command()
    async def lookup(self, context: commands.Context, target: Target) -> None:
        """
        Lookup articles that match a specific keyword.

        This is a bit harder to get results with, but it might be the case that some unlisted results do pop up!
        """
        results = {result.result_id for result in self.__search_results if target.lower() in result.keywords}
        if results:
            message = "The following articles have been found: \n"
            message += "\n".join(results)
            await context.send(message)
        else:
            await context.send("No articles matching your query were found!")


class SearchResult:
    def __init__(self, path: Path) -> None:
        self.__storage = Storage(path)
        self.__search_result_id = path.with_suffix("").name.lower()

    async def load(self):
        await self.__storage.load()

    @property
    def result_id(self):
        return self.__search_result_id

    @property
    def formatted_text(self):
        name = self.__storage.data["name"]
        raw_text = self.__storage.data["basic_info"]
        random_time = random.randint(0, 100)
        formatted_text = f"""```
The requested oogle scan is complete.
Your scan took <0.0{random_time}> ms to complete.

The scan yielded the following information:

{name}
{raw_text}
```"""
        return formatted_text

    @property
    def is_public(self):
        return bool(self.__storage.data.get("public", True))

    @property
    def keywords(self) -> List[str]:
        keywords = self.__storage.data.get("keywords", [])
        return [keyword.lower() for keyword in keywords]
