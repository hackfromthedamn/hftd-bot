from .DefaultImports import *

from contextlib import contextmanager

import weakref
import inspect

class Signal:
    def __init__(self):
        self.__bound_signals = weakref.WeakKeyDictionary()

    def __get__(self, obj, type = None):
        if obj not in self.__bound_signals:
            self.__bound_signals[obj] = BoundSignal()

        return self.__bound_signals[obj]

class BoundSignal:
    def __init__(self):
        self.__functions = weakref.WeakSet()
        self.__methods = weakref.WeakKeyDictionary()
        self.__blocked = False

    async def __call__(self, *args, **kwargs):
        if self.__blocked:
            return

        for func in self.__functions:
            await func(*args, **kwargs)

        for instance, functions in self.__methods.items():
            for func in functions:
                await func(instance, *args, **kwargs)

    async def emit(self, *args, **kwargs):
        await self(*args, **kwargs)

    def connect(self, slot):
        if inspect.ismethod(slot):
            if slot.__self__ not in self.__methods:
                self.__methods[slot.__self__] = set()

            self.__methods[slot.__self__].add(slot.__func__)
        else:
            self.__functions.add(slot)

    def disconnect(self, slot):
        if inspect.ismethod(slot):
            if slot.__self__ in self.__methods:
                self.__methods[slot.__self__].remove(slot.__func__)
        else:
            if slot in self.__functions:
                self.__functions.remove(slot)

    def clear(self):
        self.__functions.clear()
        self.__methods.clear()

    @contextmanager
    def block(self):
        self.__blocked = True
        yield self
        self.__blocked = False
