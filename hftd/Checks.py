from discord.ext import commands

def check_admin():
    async def predicate(context):
        if context.bot.is_admin(context):
            return True
        raise commands.MissingRole(context.bot.admin_role)
    return commands.check(predicate)
