from typing import List
import logging
import discord
from discord.ext import commands
log = logging.getLogger("Room")


class Room:
    def __init__(self, name: str, members: List[discord.Member], category: str) -> None:
        """
        A 'Room' is a short lived set of channels where a specific set of users have access to.
        Examples of this are active servers (they have a text & voice channel) or meetingrooms (only text).
        :param name:
        :param members:
        """
        self._name = name
        self._members = members
        self._channel_wrappers = []
        self._category = category
        self._room_category = None

    @property
    def name(self) -> str:
        return self._name

    @property
    def members(self) -> List[discord.Member]:
        return self._members

    async def _add_initial_members(self, context: commands.Context):
        members = self._members
        self._members = []
        await self.add_members(context, members)

    async def start(self, context: commands.Context) -> None:
        self._room_category = discord.utils.find(lambda c: c.name == self._category, context.guild.categories)
        if not self._room_category:
            log.error(f"Could not find room category {self._category}")

    async def stop(self, context: commands.Context) -> None:
        for wrapper in self._channel_wrappers:
            await wrapper.stop()

    async def add_members(self, context, members: List[discord.Member]) -> None:
        for member in members:
            if member in self._members:
                await context.send(f"{member.display_name} is already part of {self._name}")
                continue

            self._members.append(member)
            await context.send(f"Added {member.display_name} to {self._name}")

        await self._add_channel_permisisons(members)

        log.info(f"Added members {','.join(map(lambda m: m.display_name, members))} to {self._name}")

    async def remove_members(self, context, members: List[discord.Member]) -> None:
        for member in members:
            if member not in self._members:
                context.send(f"{member.display_name} is not part of {self._name}")
                continue

            self._members.remove(member)
        await self._remove_channel_permisisons(members)

    async def _add_channel_permisisons(self, members: List[discord.Member]):
        for wrapper in self._channel_wrappers:
            await wrapper.add_permissions(members)


    async def _remove_channel_permisisons(self, members: List[discord.Member]):
        for wrapper in self._channel_wrappers:
            await wrapper.remove_permissions(members)
