from .DefaultImports import *

from . import bot
from . import characters

from .Storage import Storage
from .Serializable import Serializable

log = logging.getLogger(__name__)
log.setLevel(logging.DEBUG)

GeneralStorage = NewType("GeneralStorage", Storage)
Token = NewType("Token", str)

async def get_prefix(bot, message):
    if not message.guild:
        return ("!", "")

    return commands.when_mentioned_or("!")(bot, message)

@injector.singleton
class Bot(commands.Bot):
    @injector.inject
    def __init__(self, token: Token, storage: GeneralStorage):
        super().__init__(
            command_prefix = get_prefix,
            case_insensitive = True,
            help_command = Help()
        )
        self.__admin_role = None
        self.__token = token
        self.__general_storage = storage
        self.__restarting = False

    @property
    def admin_role(self) -> discord.Role:
        return self.__admin_role

    @property
    def restarting(self) -> bool:
        return self.__restarting

    @restarting.setter
    def restarting(self, restart: bool) -> None:
        self.__restarting = True

    def add_cogs(self, *cogs) -> None:
        for cog in cogs:
            self.add_cog(cog)

    def run(self):
        super().run(self.__token)

    def find_member(self, to_find: Union[discord.User, str]) -> Optional[discord.Member]:
        member = None
        for guild in self.guilds:
            if isinstance(to_find, str):
                member = guild.get_member_named(to_find)
            else:
                member = guild.get_member(to_find.id)

            if member:
                break

        return member

    def is_admin(self, context: commands.Context) -> bool:
        if context.channel.permissions_for(context.author).administrator:
            return True

        member = self.find_member(context.author)
        if member:
            for role in member.roles:
                if role == self.__admin_role:
                    return True

        return False

    async def on_ready(self):
        await self.__general_storage.load()

        log.info("Guilds")
        if not self.__admin_role:
            for guild in self.guilds:
                log.info(f"- {guild}")
                self.__admin_role = discord.utils.find(lambda r: r.name == self.__general_storage.data["admin_role"], guild.roles)
                if self.__admin_role:
                    break
            else:
                log.warning("Unable to find admin role for the bot!")

        log.info("Active Cogs:")
        for name, cog in self.cogs.items():
            log.info(f"- {name}")

            if isinstance(cog, Serializable):
                await cog.load()

        try:
            self.save_all.start()
        except RuntimeError:
            pass

        log.info("Bot started and ready.")

    @tasks.loop(minutes = 1, count = None)
    async def save_all(self):
        await self.__general_storage.save()

        for name, cog in self.cogs.items():
            if isinstance(cog, Serializable):
                await cog.save()

    async def on_command_error(self, context: commands.Context, exception: commands.CommandError):
        if not isinstance(exception, commands.CommandNotFound) and not isinstance(exception, commands.CheckFailure):
            await context.send(str(exception))
        elif isinstance(exception, commands.CommandNotFound):
            await context.send("%s. Please check !help for available commands" % str(exception))

        raise exception

class Help(commands.DefaultHelpCommand):
    def get_ending_note(self):
        command_name = self.invoked_with
        return "Type {0}{1} <category> <command> for more info on a command.\n" \
               "You can also type {0}{1} <category> for more info on a category.".format(self.clean_prefix, command_name)

    async def send_bot_help(self, mapping):
        ctx = self.context
        bot = ctx.bot

        if bot.description:
            self.paginator.add_line(bot.description, empty=True)

        all_commands = []
        for command in bot.commands:
            if not isinstance(command, commands.Group):
                all_commands.append(command)
            else:
                all_commands.extend(command.commands)
        max_size = self.get_max_size(all_commands)

        filtered = await self.filter_commands(bot.commands, sort=self.sort_commands)
        to_add = []
        for command in filtered:
            if isinstance(command, commands.Group):
                group_commands = await self.filter_commands(command.commands, sort=self.sort_commands)
                self.add_indented_commands(group_commands, heading=command.name, max_size = max_size)
            else:
                to_add.append(command)

        self.paginator.add_line()

        for command in to_add:
            self.add_indented_commands([command], heading=self.commands_heading, max_size = max_size)

        note = self.get_ending_note()
        if note:
            self.paginator.add_line()
            self.paginator.add_line(note)

        await self.send_pages()
