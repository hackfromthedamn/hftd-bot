
import warnings
warnings.filterwarnings("ignore", category=DeprecationWarning)

from .Bot import Bot, Token, GeneralStorage
from .Storage import Storage, DataPath, ValueStorage
from .Message import Message
from . import Checks

from . import cogs

from . import servers

from . import meetingrooms

import injector

class Module(injector.Module):
    @injector.singleton
    @injector.provider
    def general_storage(self, data_path: DataPath) -> GeneralStorage:
        Storage.data_path = data_path
        return Storage(data_path.joinpath("general.yml"))

    def configure(self, binder: injector.Binder):
        binder.install(cogs.Module)
