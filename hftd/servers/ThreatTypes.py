from ..DefaultImports import *

from ..Storage import Storage

@dataclass(frozen = True)
class ThreatAction:
    offset: int
    action: str
    value: Any
    target: Optional[str]

    def report(self, tick: int, threat: "Threat") -> str:
        report = f"At the end of T{tick:0>2} "

        if self.action == "trace_route":
            report += f"trace route {threat.target} will lose {self.value} node(s)."
        elif self.action == "trace_route_all":
            report += f"all trace routes will lose {self.value} node(s)."
        elif self.action == "trace_route_all_even_if_disconnected":
            report += f"all trace routes will lose {self.value} node(s), even if all QAIs are disconnected."
        elif self.action == "trace_route_all_if_damaged":
            report += f"all trace routes will lose {self.value} node(s), only if damaged."
        elif self.action == "trace_route_QPU":
            report += f"trace routes {threat.target} will lose as much node(s) as there is QPU in {threat.port}."
        elif self.action == "trace_route_all_but_target":
            report += f"all trace routes except trace route {threat.target} will lose {self.value} node(s)."
        elif self.action == "heal":
            report += f"this program will heal {self.value} damage."
        elif self.action == "heal_half":
            report += f"this AI will heal half it's damage rounded down."
        elif self.action == "damage_resist":
            report += f"it ignores the first {self.value} damage per tick."
        elif self.action == "scanned":
            report += "this program is scanned down."
        elif self.action == "drain_qpu":
            report += f"port {threat.port} will lose {self.value} QPU."
        elif self.action == "drain_qpu_all":
            report += f"all ports will lose {self.value} QPU."
        elif self.action == "create_security_log":
            report += f"the security log is copied to Port {threat.target}. Edit its data once to edit all your activities out."
        elif self.action == "connect_port":
            report += f"this AI connects to Port {threat.move}."
        elif self.action == "connect_port_second":
            report += f"this AI connects to Port {threat.move_second}."
        elif self.action == "spread_port":
            report += f"this AI spreads to Port {threat.move} and needs to be deleted there once too to stop it."
        elif self.action == "connect_if_damaged":
            report += f"this AI connects to Port {threat.move_second} if damaged, otherwise trace route {threat.target} will lose {self.value} node(s)."
        elif self.action == "trace_route_and_kick":
            report += f"trace route {threat.target} will lose {self.value} node(s) and AI's in the port this AI is connected to are immediately disconnected."
        elif self.action == "wake_up":
            report += f"from this point on if you damage this AI, it will lower your access level by 1 level."
        elif self.action == "connects_and_traces_nearby_ai":
            report += f"this AI connects to Port {threat.move} and trace route {threat.target} will lose as many nodes as there are hostile AI's connected to Port {threat.move}"
        elif self.action == "trace_route_all_and_delete_ai":
            report += f"all trace routes will lose {self.value} node(s) and all AI's on the server will be immediately deleted."
        elif self.action == "kick_superusers":
            report += f"all AI's with the access level of superuser are immediately disconnected."
        elif self.action == "kick_access_level_ports":
            report += f"all AI's in ports that can increase user levels are immediately disconnected."
        elif self.action == "kick_all_but_initial_connect":
            report += f"all AI's that are not in inital connect ports are immediately disconnected."
        elif self.action == "hide":
            report += f"this program goes into hiding and can't be targeted next tick."
        elif self.action == "scout":
            report += f"until this threat is destroyed all treats trace {self.value} extra node per attack per route."
        elif self.action == "scout_damage_resist":
            report += f"until this threat is destroyed all treats that ignore damage per tick, ignore {self.value} extra."
        elif self.action == "lower_uses":
            report += f"any action on this port that can only be performed a limited time per hack loses {self.value} uses."
        elif self.action == "lower_link_uses_all":
            report += f"all link actions lose {self.value} uses."
        elif self.action == "disable_access_level":
            report += f"this port can no longer increase user access level (but can still support it)."
        elif self.action == "kick_on_port":
            report += f"all AI's on {threat.port} are immediatly disconnected."
        elif self.action == "kick_on_port_second":
            report += f"all AI's on {threat.port_second} are immediatly disconnected."
        elif self.action == "tag_AI_in_adjecent_port":
            report += f"all AI's on ports that port {threat.port} can connect to are tagged."
        elif self.action == "kick_tagged":
            report += f"all tagged AI's are instantly disconnected."
        elif self.action == "delay_external_AI":
            report += f"all actions of all external AI's are performed 1 tick later."
        return report

@dataclass(frozen = True)
class ThreatType:
    name: str
    initial: str
    health: int
    damage_resist: int
    steps: List[ThreatAction]

class ThreatTypes:
    def __init__(self, path: Optional[Path] = None):
        if not path:
            path = Storage.data_path.joinpath("threats.yml")

        self.__storage = Storage(path)
        self.__threats = {}

    @property
    def count(self) -> int:
        return len(self.__threats)

    @property
    def types(self) -> List[str]:
        return list(self.__threats.keys())

    def get(self, threat_type: str) -> Optional[ThreatType]:
        return self.__threats.get(threat_type, None)

    async def load(self) -> None:
        await self.__storage.load()

        self.__threats = self.from_dict(self.__storage.data)

    @classmethod
    def from_dict(self, data: Dict[str, Any]):
        threats = {}
        for type_id, data in data.items():
            steps = []
            for step in data.get("steps", []):
                steps.append(ThreatAction(
                    offset = step["time"],
                    action = step["action"],
                    value = step.get("value", None),
                    target = step.get("target", None)
                ))

            threats[type_id] = ThreatType(
                name = data["name"],
                initial = data["initial"],
                health = data.get("health", -1),
                damage_resist = data.get("damage_resist", 0),
                steps = steps
            )

        return threats
