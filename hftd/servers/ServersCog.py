from ..DefaultImports import *

from ..Storage import Storage
from .. import Checks
from ..Serializable import Serializable

import hftd

from .ActiveServer import ActiveServer
from .Server import Server
from .ThreatTypes import ThreatTypes

log = logging.getLogger("Servers")


class ServersCog(Serializable, commands.Cog, name = "Servers"):
    def __init__(self, bot: commands.Bot, general_storage: hftd.GeneralStorage) -> None:
        self.bot = bot

        self.__threat_types = ThreatTypes()

        self.__servers = {}
        self.__active_servers = {}
        self.__server_counters = {}
        self.__general_storage = general_storage

    @property
    def active_server_count(self):
        return len(self.__active_servers)

    @commands.Cog.listener()
    async def on_message(self, message: discord.Message) -> None:
        if message.channel.type != discord.ChannelType.text:
            return

        for server in self.__active_servers.values():
            await server.process_message(message)

    @commands.group()
    @Checks.check_admin()
    async def server(self, context):
        """
        Commands that deal with servers.

        A server can be "active", in which case it is running, has one or more
        associated channels and accepts input to those channels.

        Active servers have a "type", which is a template that describes the messages
        that will be sent in response to commands.

        Active servers have "members", which are people that are required to run a plan
        for the server to complete. They can also have "spectators", which are people
        that can read the server's channels but that are not required to submit a plan
        for the server to complete.
        """
        pass

    @server.command()
    async def start(self, context, server_name: str, members: commands.Greedy[discord.Member]) -> None:
        """
        Start a new server.

        This will start a new active server of the specified type.

        :param server_name: The name of the server type to start the new server from.
        :param members: The members that should be added to the new server.
        """
        if server_name not in self.__servers:
            await context.send("No such server.")
            return

        active_server_name = f"{server_name}-{self.__server_counters[server_name]}"
        self.__server_counters[server_name] += 1

        active_server = ActiveServer(self.bot, self.__servers[server_name], active_server_name, members,
                                     self.__general_storage.data["server_category"])
        await active_server.start(context)
        self.__active_servers[active_server_name] = active_server

        await context.send(f"Started server {active_server_name}")

    @server.command()
    async def stop(self, context, server_name: str) -> None:
        """
        Stop an active server.

        :param server_name: The name of an active server.
        """
        if server_name not in self.__active_servers:
            await context.send("No such active server.")
            return

        await self.__active_servers[server_name].stop(context)
        del self.__active_servers[server_name]

        await context.send(f"Stopped server {server_name}")

    @server.command()
    async def list(self, context) -> None:
        """
        List all available server types.
        """
        await context.send(f"{len(self.__servers)} servers")
        for name in self.__servers:
            await context.send(f"- {name}")

    @server.command(name = "list-active", aliases = ["list_active"])
    async def list_active(self, context) -> None:
        """
        List all active servers.
        """
        await context.send(f"{len(self.__active_servers)} active servers.")
        for name in self.__active_servers:
            await context.send(f"- {name}")

    @server.command()
    async def status(self, context, server_name: str) -> None:
        """
        Show the status of an active server.

        This will display information about the server. If it has finished, this
        will instead generate a partial AAR.

        :param server_name: The name of the server to display the status of.
        """
        if server_name not in self.__active_servers:
            await context.send("No such active server.")
            return

        await self.__active_servers[server_name].status(context)

    @server.command(name = "add-members", aliases = ["add_members"])
    async def add_members(self, context, server_name: str, members: commands.Greedy[discord.Member]) -> None:
        """
        Add more members to a server.

        :param server_name: The server to add members to.
        :param members: The extra members to add.
        """
        if server_name not in self.__active_servers:
            await context.send("No such active server.")
            return

        await self.__active_servers[server_name].add_members(context, members)

    @server.command(name = "remove-members", aliases = ["remove_members"])
    async def remove_members(self, context, server_name: str, members: commands.Greedy[discord.Member]) -> None:
        """
        Remove members from a server.

        :param server_name: The server to remove members from.
        :param members: The members to remove.
        """
        if server_name not in self.__active_servers:
            await context.send("No such active server.")
            return

        await self.__active_servers[server_name].remove_members(context, members)

    @server.command(name = "add-spectators", aliases = ["add_spectators"])
    async def add_spectators(self, context, server_name: str, members: commands.Greedy[discord.Member]) -> None:
        """
        Add spectators to a server.

        :param server_name: The server to add spectators to.
        :param members: The spectators to add.
        """
        if server_name not in self.__active_servers:
            await context.send("No such active server.")
            return

        await self.__active_servers[server_name].add_spectators(context, members)

    @server.command(name = "remove-spectators", aliases = ["remove_spectators"])
    async def remove_spectators(self, context, server_name: str, members: commands.Greedy[discord.Member]) -> None:
        """
        Remove spectators from a server.

        :param server_name: The server to remove spectators from.
        :param members: The spectators to remove.
        """
        if server_name not in self.__active_servers:
            await context.send("No such active server.")
            return

        await self.__active_servers[server_name].remove_spectators(context, members)

    async def load(self):
        await self.__threat_types.load()

        path = Storage.data_path.joinpath("servers")
        for file_path in path.iterdir():
            if not file_path.match("*.yml"):
                continue

            try:
                server = Server(self.__threat_types, file_path)
                await server.load()
            except Exception as e:
                log.exception(f"An exception occurred when loading server {file_path.name}")
                continue

            self.__servers[server.server_id] = server
            self.__server_counters[server.server_id] = 0
