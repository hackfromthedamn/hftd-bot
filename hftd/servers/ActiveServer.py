import logging
from typing import List, Dict
import datetime
import copy

import discord
from discord.ext import commands
from discord.ext import tasks

from ..ChannelWrapper import ChannelWrapper
from ..Room import Room
from ..Storage import Storage
from ..Message import Message

from .Server import Server
from .Command import InvalidInputError, Command
from .Report import Report
from hftd.Room import Room
log = logging.getLogger("ActiveServer")


class ActiveServer(Room):
    def __init__(self, bot, server: Server, name: str, members: List[discord.Member], category: str) -> None:
        super().__init__(name, members, category)
        self.__bot = bot
        self.__server = server
        self.__text_channel = None
        self.__text_channel_wrapper = None
        self.__voice_channel_wrapper = None
        self.__plans:Dict[discord.Member, List[Command]] = {}
        self.__planning = set()
        self.__finished = []
        self.__log_file = None
        self.__elapsed_time = datetime.timedelta(seconds = 0)
        self.__threats = copy.deepcopy(server.threats)
        self.__scanning_started = False
        self.__server_finished = False

        self.__countdown = tasks.loop(seconds = 10, count = None)(self.countdown)
        self.__countdown.after_loop(self.countdown_end)

    @property
    def name(self) -> str:
        return self._name

    @property
    def server(self) -> Server:
        return self.__server

    @property
    def members(self) -> List[discord.Member]:
        return self._members

    @property
    def elapsed(self) -> datetime.time:
        return self.__elapsed_time

    @property
    def plans(self) -> Dict[discord.Member, List[Command]]:
        return self.__plans

    @property
    def priority(self) -> List[discord.Member]:
        return self.__finished

    @property
    def threats(self) -> List["Threat"]:
        return self.__threats

    async def start(self, context: commands.Context) -> None:
        await super().start(context)

        overwrites = {
            context.guild.default_role: discord.PermissionOverwrite(
                read_messages = False, view_channel = False
            ),
            self.__bot.admin_role: discord.PermissionOverwrite(
                read_messages = True, view_channel = True
            )
        }

        self.__text_channel = await context.guild.create_text_channel(self.name, category = self._room_category, overwrites = overwrites)
        self._channel_wrappers.append(ChannelWrapper(self.__text_channel))

        voice_channel = await context.guild.create_voice_channel(self.name, category = self._room_category, overwrites = overwrites)
        self._channel_wrappers.append(ChannelWrapper(voice_channel))

        await self._add_initial_members(context)

        path = Storage.data_path.joinpath("logs", self.__server.server_id, datetime.datetime.utcnow().isoformat() + ".log")
        path.parent.mkdir(parents = True, exist_ok = True)
        self.__log_file = open(path, "w")

        await self.__server.welcome_message.send(self.__text_channel)

        log.info(f"Started server {self.name}")
        log.info(f"  Type: {self.__server.name}")
        log.info(f"  Members: {','.join(map(lambda m: m.display_name, self._members))}")

    async def stop(self, context: commands.Context) -> None:
        self.__log_file.close()
        self.__countdown.cancel()
        await super().stop(context)
        log.info(f"Stopped server {self.name}")

    async def status(self, context: commands.Context) -> None:
        if self.__server_finished:
            report = Report(self)
            await report.generate(context)
            return

        template = """
{{ active.name }}
Type: {{ active.server.server_id }}

Server is not finished yet.

Members:
{% for member in active.members -%}
-   {{ member.display_name }}
{% endfor %}
Time elapsed: {{ active.elapsed }}
"""
        message = Message(template).processed(active = self)
        await message.send(context.channel)

        if self.__plans:
            await context.send("### Plans")
            for member, plan in self.__plans.items():
                message = f"```\n{member.display_name}\n"
                for command in plan:
                    message += str(command) + "\n"
                message += "```\n"
                await context.send(message)

    async def remove_members(self, context, members: List[discord.Member]) -> None:
        super().remove_members(context, members)
        for member in members:
            if member in self.__finished:
                self.__finished.remove(member)

            if member in self.__planning:
                self.__planning.remove(member)

            if len(self.__plans) == len(self._members) and len(self.__planning) == 0:
                await self.__server.planning_done.send(self.__text_channel)

            if len(self.__finished) == len(self._members):
                self.__countdown.cancel()
                await self.__server.finished.send(self.__text_channel)

            await context.send(f"Removed {member.display_name} from {self.name}")

        log.info(f"Removed members {','.join(map(lambda m: m.display_name, members))} from {self.name}")

    async def add_spectators(self, context, members: List[discord.Member]) -> None:
        await self._add_channel_permisisons(members)

        for member in members:
            await context.send(f"Added {member.display_name} as a spectator to {self._name}")

    async def remove_spectators(self, context, members: List[discord.Member]) -> None:
        await self._remove_channel_permisisons(members)

        for member in members:
            await context.send(f"Removed {member.display_name} as a spectator from {self._name}")

    async def process_message(self, message: discord.Message) -> None:
        if message.channel != self.__text_channel:
            return

        cleaned = message.clean_content.strip()
        self.__log_file.write(f"[{message.created_at.isoformat()}] <{message.author.display_name}> {cleaned}\n")

        if message.author == self.__bot.user:
            return

        if message.author not in self._members:
            return

        lines = cleaned.split("\n")
        for line in lines:
            if not line.startswith("~"):
                if message.author in self.__planning:
                    try:
                        command = Command.fromString(line)
                    except InvalidInputError as e:
                        await self.__text_channel.send(f"{message.author.mention}: {e}")
                    else:
                        self.__plans[message.author].append(command)
                continue

            cleaned_line = line.lstrip("~").replace(" ", "_")

            command = getattr(self, cleaned_line, None)
            if command:
                if message.author in self.__finished:
                    await self.__text_channel.send("Your AI is executing your plan and cannot accept any new commands.")
                    return
                else:
                    await command(message)
            else:
                await self.__text_channel.send("Unknown command")

    async def initiate_server_scan(self, message: discord.Message) -> None:
        if self.__scanning_started:
            return

        await self.__server.scan_result.send(self.__text_channel)
        self.__countdown.start()
        self.__scanning_started = True

    async def begin_plan(self, message: discord.Message) -> None:
        if not self.__scanning_started:
            await self.__text_channel.send("You cannot enter your plan yet. You should first scan the server.")
            return

        self.__planning.add(message.author)
        self.__plans[message.author] = []
        await self.__text_channel.send(f"{message.author.mention}: Beginning plan")

    async def end_plan(self, message: discord.Message) -> None:
        if message.author in self.__planning:
            self.__planning.remove(message.author)
            await self.__text_channel.send(f"{message.author.mention}: Ended plan")

        if len(self.__plans) == len(self._members) and len(self.__planning) == 0:
            await self.__server.planning_done.send(self.__text_channel)

    async def plan(self, message: discord.Message) -> None:
        if message.author not in self.__plans or not self.__plans[message.author]:
            await self.__text_channel.send("You have no current plan.")
            return

        await self.__text_channel.send(f"{message.author.mention}: This is your current plan:")
        plan = "```\n"
        for command in self.__plans[message.author]:
            plan += str(command) + "\n"
        plan += "```\n"
        await self.__text_channel.send(plan)

    async def run(self, message: discord.Message) -> None:
        if message.author not in self.__plans:
            await self.__text_channel.send("You have no plan to run.")
            return

        if message.author in self.__planning:
            await self.__text_channel.send("You are still planning. Finish your plan with `~end plan`.")
            return

        self.__finished.append(message.author)
        await self.__text_channel.send(f"{message.author.mention}: Your QAI is now scheduled to execute its plan.")

        if len(self.__finished) == len(self._members):
            self.__countdown.cancel()
            self.__server_finished = True
            await self.__server.finished_success.send(self.__text_channel)

    async def help(self, message: discord.Message) -> None:
        message = """```
Known commands:

~initiate server scan
    Begin scanning a server.
~begin plan
    Start writing a plan.
    All messages you send after this become part of your plan.
    If you already had a plan, it will be erased.
~end plan
    Finish writing your plan.
~plan
    Display your current plan.
~run
    Run your current plan.
    After this you will not be able to enter any more commands.
```"""
        await self.__text_channel.send(message)

    async def countdown(self):
        self.__elapsed_time += datetime.timedelta(seconds = 10)
        if self.__elapsed_time >= self.__server.duration:
            self.__countdown.stop()

        for threat in self.__threats:
            if threat.report_time <= self.__elapsed_time and not threat.reported:
                await threat.report.send(self.__text_channel)
                threat.reported = True

        if self.__elapsed_time.seconds % 60 == 0:
            remaining = (self.__server.duration - self.__elapsed_time).seconds // 60
            await self.__text_channel.send(f"{remaining} minutes remaining")

    async def countdown_end(self):
        if self.__server_finished:
            return

        for member in self._members:
            if member not in self.__finished:
                self.__finished.append(member)

        self.__server_finished = True
        await self.__server.finished_timeout.send(self.__text_channel)

