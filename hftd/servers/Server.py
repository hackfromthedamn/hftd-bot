from ..DefaultImports import *

from ..Storage import Storage
from ..Message import Message

from .Threat import Threat
from .ThreatTypes import ThreatTypes, ThreatType, ThreatAction

log = logging.getLogger("Server")

class Server:
    def __init__(self, threat_types: ThreatTypes, path: Path) -> None:
        self.__threat_types = threat_types

        self.__server_file = Storage(path)
        self.__server_id = path.with_suffix("").name

        self.__duration = datetime.timedelta(minutes = 11)

        self.__welcome_message = None
        self.__scan_result = None
        self.__planning_done = None
        self.__finished_success = None
        self.__finished_timeout = None

        self.__threats = []

    @property
    def server_id(self) -> str:
        return self.__server_id

    @property
    def name(self) -> str:
        return self.__server_file.data.get("name", self.__server_id)

    @property
    def duration(self) -> datetime.timedelta:
        return self.__duration

    @property
    def maximum(self) -> int:
        return self.__server_file.data.get("maximum", -1)

    @property
    def welcome_message(self) -> Message:
        return self.__welcome_message

    @property
    def scan_result(self) -> Message:
        return self.__scan_result

    @property
    def planning_done(self) -> Message:
        return self.__planning_done

    @property
    def finished_success(self) -> Message:
        return self.__finished_success

    @property
    def finished_timeout(self) -> Message:
        return self.__finished_timeout

    @property
    def threats(self):
        return self.__threats

    async def load(self):
        await self.__server_file.load()

        if "duration" in self.__server_file.data:
            parts = self.__server_file.data["duration"].split(":")
            self.__duration = datetime.timedelta(minutes = int(parts[0]), seconds = int(parts[1]))

        self.__welcome_message = Message(self.__server_file.data.get("welcome_message", ""))
        self.__scan_result = Message(self.__server_file.data.get("scan_result", ""))
        self.__planning_done = Message(self.__server_file.data.get("planning_done", ""))
        self.__finished_success = Message(self.__server_file.data.get("finished_success", ""))
        self.__finished_timeout = Message(self.__server_file.data.get("finished_timeout", ""))

        extra_threats = ThreatTypes.from_dict(self.__server_file.data.get("threat_types", {}))

        for entry in self.__server_file.data.get("threats", []):
            threat_type = self.__threat_types.get(entry["type"])
            threat_type = extra_threats.get(entry["type"], threat_type)

            if not threat_type:
                log.warn(f"{self.__server_file.path.name}: Unknown threat type '{entry['type']}'")
                continue

            self.__threats.append(Threat(self, threat_type, entry))
