#!/usr/bin/env python

import datetime
import os
import logging
import asyncio
import pathlib
import sys

import dotenv

import hftd

logging.basicConfig(level = logging.INFO)

dotenv.load_dotenv()
if os.getenv("BOT_DATA_PATH"):
    hftd.Storage.data_path = pathlib.Path(os.getenv("BOT_DATA_PATH"))

threats = hftd.servers.ThreatTypes()

start = 1
security_system = "Security System 1"
target = 1
port = 1

if len(sys.argv) > 1:
    start = int(sys.argv[1])
if len(sys.argv) > 2:
    security_system = sys.argv[2]
if len(sys.argv) > 3:
    target = sys.argv[3]
if len(sys.argv) > 4:
    port = sys.argv[4]

async def main():
    await threats.load()

    for type_name in threats.types:
        data = {
            "time": "0:00",
            "security_system": security_system,
            "target": target,
            "start": start,
            "port": port
        }

        threat_type = threats.get(type_name)
        threat = hftd.servers.Threat(None, threat_type, data)

        print(threat.report.message_parts[0])

asyncio.run(main())
