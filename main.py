#!/usr/bin/env python

import os
import logging
import pathlib

import dotenv

import hftd

logging.basicConfig(level = logging.INFO)

dotenv.load_dotenv()
discord_token = os.getenv('DISCORD_TOKEN')

if os.getenv("BOT_DATA_PATH"):
    hftd.Storage.data_path = pathlib.Path(os.getenv("BOT_DATA_PATH"))

bot = hftd.Bot()
bot.run(discord_token)
