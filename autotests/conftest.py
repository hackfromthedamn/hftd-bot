from pathlib import Path

import pytest
import hftd

@pytest.fixture
async def threat_types():
    types = hftd.servers.ThreatTypes(Path(__file__).with_name("threats.yml"))
    await types.load()
    yield types
