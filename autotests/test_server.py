from pathlib import Path
import datetime

import pytest
import hftd

@pytest.mark.asyncio
async def test_server(threat_types):
    server = hftd.servers.Server(threat_types, Path(__file__).with_name("server.yml"))
    await server.load()

    assert server.name == "Test Server"
    assert server.duration == datetime.timedelta(minutes = 11, seconds = 11)
    assert server.maximum == 5

    assert server.welcome_message == hftd.Message("Test server")
    assert server.scan_result == hftd.Message("Test scan result")
    assert server.finished_success == hftd.Message("Test finished successful")
    assert server.finished_timeout == hftd.Message("Test timeout")

    assert len(server.threats) == 2

    threat0 = server.threats[0]
    assert threat0.report_time == datetime.timedelta(seconds = 30)
    assert threat0.type == threat_types.get("test_server_type")
    assert threat0.start == 2
    print(threat0.report)
    assert threat0.report == hftd.Message("```\n### Threat Detected: Test Server Type ###\nInitial message for Test Server at 2\n```")

    step = threat0.steps[4]
    assert step.action == "test"

    threat1 = server.threats[1]
    assert threat1.report_time == datetime.timedelta(minutes = 2)
    assert threat1.start == 4

    assert threat1.steps[6]
