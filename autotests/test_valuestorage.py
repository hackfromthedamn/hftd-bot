import pytest

import pathlib

from decimal import Decimal
import hftd

@pytest.mark.asyncio
async def test_valuestorage():
    storage = hftd.ValueStorage(pathlib.Path(__file__).with_name("test_valuestorage.yml"), pathlib.Path(__file__).with_name("test_valuestorage.log"))
    await storage.load()

    await storage.set("test", Decimal(10))
    assert storage.value("test") == Decimal(10)

    await storage.modify("test", Decimal(10))
    assert storage.value("test") == Decimal(20)

    await storage.save()

    storage = hftd.ValueStorage(pathlib.Path(__file__).with_name("test_valuestorage.yml"), pathlib.Path(__file__).with_name("test_valuestorage.log"))
    await storage.load()

    assert storage.value("test") == Decimal(20)
